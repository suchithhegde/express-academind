const express = require('express');

const app = express();

app.use('/', (req,res,next) => {
    console.log('Start session now');
    next();
});

app.use('/users',(req,res,next) => {
    res.send('<h1>You are welcome to my page</h1>');
});

app.use('/', (req,res,next) => {
    console.log('initialize default path');
    res.send('<h1>Select a resource path!</h1>');    
});

app.listen(3000);

console.log('Listening on port 3000........');
